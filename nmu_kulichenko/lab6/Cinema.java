package com.nmu.kulichenko.lab6;

/**
 * Created by user on 10.12.2016.
 */
public class Cinema{
    String name;
    String city;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Cinema cinema = (Cinema) o;

        if (!name.equals(cinema.name)) return false;
        return name.equals(cinema.name);
    }

}
