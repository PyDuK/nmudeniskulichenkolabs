package com.nmu.kulichenko.lab6;

/**
 * Created by user on 10.12.2016.
 */
public class Movie {
    String moviemaker;
    String name;
    Integer ticketprice;
    Cinema cinema;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Movie movie = (Movie) o;

        if (!moviemaker.equals(movie.moviemaker)) return false;
        if (!name.equals(movie.name)) return false;
        if (!ticketprice.equals(movie.ticketprice)) return false;
        return cinema.equals(movie.cinema);
    }


    @Override
    public int hashCode() {
        int result = moviemaker.hashCode();
        result = 31 * result + name.hashCode();
        result = 31 * result + ticketprice.hashCode();
        return result;
    }
}