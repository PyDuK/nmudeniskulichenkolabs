package com.nmu.kulichenko.lab6;
import com.google.gson.Gson;
/**
 * Created by user on 10.12.2016.
 */
public class Labrab6 {
    public static void main(String[] args) {
        Movie movie= new Movie();
        movie.moviemaker="Marvel";
        movie.name="Thor:Ragnarok";
        movie.ticketprice= 10;
        Cinema cinema=new Cinema();
        cinema.name="RegentStreetCinema";
        cinema.city="London";
        movie.cinema=cinema;
        Gson gson=new Gson();
        String json=gson.toJson(movie);
        System.out.println(json);
        Movie studentFromJson= new Movie();
        gson.fromJson(json,Movie.class);
    }

}


